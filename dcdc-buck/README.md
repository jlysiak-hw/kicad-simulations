# Buck DC/DC converter simulation

## Schematic

![sch][sch]

## Result

![result][result]


[result]:./imgs/dcdc.png
[sch]:./imgs/sch.png
