# KiCAD & ngspice simulations

## Simple N-MOSFET circuit simulation

Simulation of basic circuit with N-Channel MOSFET which:
- Drain is connected to supply voltage `V1: 0...30V`
- Gate is biased with `V2: 5V`
- Source is connected to GND via `22R` resistor
The schematic:
![schematic][sch]

I've created this because:
- I'm recently learning how to build own regulated power supply and this is just
  its small piece that I wanted to understand
- I wanted to learn how to perform some electronic circuit simulations on Linux
  with KiCAD and ngspice.
- I had a couple of issues with running the sim in KiCAD, so you can learn how
  to overcome the issues faster.

## Result

Here is the result. Basically, at the beginning `Q1` is fully closed (channel
resistance is very small), so `V1` is actually equal to `U_R2`, and when
`U_gs = V2 - U_R2` becomes small enough, `Q1` channel's resistance increases.
At some point, all additional voltage is kept by transistor `Q1` and the current
is almost doesn't change (with respect to changing `V1` voltage of course)

![result][result]

The plot shows both voltage and current flowing through resistor `R2` but
,actually, lines exactly overlaps because `U = I x R` and voltage axis is just
current axis scaled, so the shape of the plot stays the same.


## Issues with KiCad and ngspice

To simulate this circuit I had perform the following steps:
1. Create the schematic in KiCad using
    - `pspice` resistor elements
    - `pspice` voltage source elements
    - `0` voltage reference
    - `Q_NMOS_DGS` element for MOSFET (but, element itself doesn't have
      SPICE model :(, we need to get it separately)
2. Download SPICE models for KiCAD from [KiCAD SPICE library][lib]
3. Find `KiCad-Spice-Library/Models/Transistor/FET/MOS.lib` open it and copy the
   line containing IRF6618 MOSFET model description. Paste it to some file in
   a location you want, i.e. your simulation workspace in `some-file.lib`.
5. Now edit this line:
```
.model IRF6618 VDMOS(Rg=2 Rd=1.1m Rs=0 Vto=2.2 lambda=.02 Kp=150 mtriode=1.5 Cgdmax=2.2n Cgdmin=.2n Cgs=6n Cjo=.92n Is=50p Rb=1.5m mfg=International_Rectifier Vds=30 Ron=2.8m Qg=43n)
```
and make it look like this
```
.MODEL IRF6618 VDMOS Rg=2 Rd=1.1m Rs=0 Vto=2.2 lambda=.02 Kp=150 mtriode=1.5
+ Cgdmax=2.2n Cgdmin=.2n Cgs=6n Cjo=.92n Is=50p Rb=1.5m
+ mfg=International_Rectifier Vds=30 Ron=2.8m Qg=43n
```
So, just remove parenthesis and to keep rows short move description to next
lines marking a continuation with `+`.

4. In KiCAD, click (right mouse button) on the transistor and select
   `Properties`, click on `Select model`, select your one liner file and choose
   your model. In element value field append `IRF6618` to `Q_NMOS_DGS`.
5. Now you can hit `Tools>Simulator`, in `Settings` select`DC Transfer` tab and
   for `DC sweep source 1` set:
```
DC source: V1
Starting voltage: 0
Final voltage: 30V
Increment step: 0.1V
```
and confirm.

6. Now click `Run simulation` and once it's done, click `Add signals` and select
`I(R2)`. You should see something like the plot above.

The issue I initially had was crashing KiCad when I just loaded a MOSFET model
directly from `MOS.lib` and selected `IRF6618`. Basically, KiCAD was generating
a correct circuit description but `ngspice` was surprisingly crashing with
message like `** stack smash detected **` and the issue was disappearing when
I deleted `include` command from `*.cir` file. So, it looked like models
description format is wrong in files I found in [repo][lib]. I figured out that
in ngspice manual they don't use parenthesis in `VDMOS` model description, so I
changed it and it worked. :wink:


If you want to understand KiCAD `*.cir` files and elements models description
format and physical parameters included in models I recommend to go briefly
through [ngspice manual][ngspice-man].

[sch]:imgs/schematic.png
[result]:imgs/U0_sweep_0-30V.png
[lib]:https://github.com/kicad-spice-library/KiCad-Spice-Library
[ngspice-man]:http://ngspice.sourceforge.net/docs/ngspice-manual.pdf
