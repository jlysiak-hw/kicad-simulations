# OP07 simulation

## Steps needed to make OpAmp work in KiCAD

Draw a circuit using `pspice:opamp` model. It has only `5` pin numbered:
- `1`: non-inverting input
- `2`: inverting input
- `3`: output
- `4`: positive power supply
- `5`: negative power supply

It's important to use a model that has only `5` pins, as ngspice complained a
lot when I was using the model with extra pins (like offset compensation).
That's because a SPICE model circuit used here (in `op07.lib`) uses also only
5 pins to simulate the circuit.

I draw something like this:

![circuit][circuit]

Basically, it's inverting amplifier with `k=0.1`.

Get opamp SPICE model from [KiCad-Spice-Library][lib] repository.
File `op07.lib` in this repository I copied from `KiCad-Spice-Library/Models/Operational Amplifier/Op07.mod`.
I changed `*.mod` to `*.lib` as KiCAD doesn't recognize `*.mod` files.

Go to KiCAD schematic and open opamp item properties (`E` or `RMB>Properties>Edit properties`).
Open `Edit Spice Model`. Click `Select file...` in `Model` tab and find the file
I mentioned above (or another one, if you wish so).

Select model `OP07` and `Subcircuit` type.

Line `10` there is a comment: `* Connections: + - V+V-O`  
And line `11` starts subcircuit definition: `.subckt OP07 3 2 7 4 6`  
So by default the mapping is as follows:
- `+` (non-inverting input): pin 3
- `-` (inverting input): pin 2
- `V+` (positive power supply): pin 7
- `V-` (negative power supply): pin 4
- `O` (output): pin 6

Unfortunately, the model I selected has different pinout.

![opamp][opamp-img]

As you see in the schematic above, our mapping is as follows:
- `+` (non-inverting input): pin 1
- `-` (inverting input): pin 2
- `V+` (positive power supply): pin 4
- `V-` (negative power supply): pin 5
- `O` (output): pin 3

And to remap subcircuit pins to connect correctly our model we need to check
`Alternate node sequence` and provide such a sequence: `1 2 4 5 3`

Here is the dialog with my opamp spice model configuration:

![opamp-spice][opamp-spice]

Now you should be able to hit `Tools > Simulation` to open Ngspice simulator
front-end. Enter settings and select `Transient` tab.

I provided `1u` as `Time step` and `10m` as `Final time`, so having the
following spice configuration of `IN` voltage source:

![in-config][in-config]

we should see the following result

![plot][plot]


[circuit]:./imgs/circuit.png
[plot]:./imgs/voltage-plot.png
[opamp-img]:./imgs/opamp.png
[opamp-spice]:./imgs/spice-model-dialog.png
[in-config]:./imgs/in-config.png
[lib]:https://github.com/kicad-spice-library/KiCad-Spice-Library/tree/master/Models
