EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:R R1
U 1 1 6125A2C8
P 3150 2800
F 0 "R1" V 2945 2800 50  0000 C CNN
F 1 "10k" V 3036 2800 50  0000 C CNN
F 2 "" H 3150 2800 50  0001 C CNN
F 3 "~" H 3150 2800 50  0001 C CNN
	1    3150 2800
	0    1    1    0   
$EndComp
$Comp
L pspice:R R2
U 1 1 6125A716
P 3925 3300
F 0 "R2" V 3720 3300 50  0000 C CNN
F 1 "1k" V 3811 3300 50  0000 C CNN
F 2 "" H 3925 3300 50  0001 C CNN
F 3 "~" H 3925 3300 50  0001 C CNN
	1    3925 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 2800 3550 2800
Wire Wire Line
	3550 2800 3550 3300
Wire Wire Line
	3550 3300 3675 3300
Connection ~ 3550 2800
Wire Wire Line
	3550 2800 4075 2800
Wire Wire Line
	4175 3300 5075 3300
Wire Wire Line
	5075 3300 5075 2700
Wire Wire Line
	5075 2700 4675 2700
Wire Wire Line
	2900 2800 2800 2800
Text GLabel 2800 2800 0    50   Input ~ 0
IN
Wire Wire Line
	5075 2700 5250 2700
Connection ~ 5075 2700
Text GLabel 5250 2700 2    50   Output ~ 0
OUT
Text GLabel 3900 2600 0    50   Input ~ 0
GND
Wire Wire Line
	3900 2600 4075 2600
Text GLabel 4275 2150 1    50   Input ~ 0
VP
Wire Wire Line
	4275 2150 4275 2400
Text GLabel 4275 3100 3    50   Input ~ 0
VM
Wire Wire Line
	4275 3100 4275 3000
$Comp
L pspice:VSOURCE V2
U 1 1 6125BEEF
P 4000 4325
F 0 "V2" H 4228 4371 50  0000 L CNN
F 1 "-5" H 4228 4280 50  0000 L CNN
F 2 "" H 4000 4325 50  0001 C CNN
F 3 "~" H 4000 4325 50  0001 C CNN
	1    4000 4325
	1    0    0    -1  
$EndComp
$Comp
L pspice:VSOURCE V3
U 1 1 6125C95C
P 4975 4325
F 0 "V3" H 5203 4371 50  0000 L CNN
F 1 "5" H 5203 4280 50  0000 L CNN
F 2 "" H 4975 4325 50  0001 C CNN
F 3 "~" H 4975 4325 50  0001 C CNN
	1    4975 4325
	1    0    0    -1  
$EndComp
$Comp
L pspice:VSOURCE V1
U 1 1 6125CDE1
P 3000 4325
F 0 "V1" H 3228 4371 50  0000 L CNN
F 1 "dc 0 ac 1" H 3228 4280 50  0000 L CNN
F 2 "" H 3000 4325 50  0001 C CNN
F 3 "~" H 3000 4325 50  0001 C CNN
F 4 "V" H 3000 4325 50  0001 C CNN "Spice_Primitive"
F 5 "dc 0 ac 1 sin(0 1 1000 0)" H 3000 4325 50  0001 C CNN "Spice_Model"
F 6 "Y" H 3000 4325 50  0001 C CNN "Spice_Netlist_Enabled"
	1    3000 4325
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND01
U 1 1 6125D5A1
P 3000 5275
F 0 "#GND01" H 3000 5175 50  0001 C CNN
F 1 "0" H 3000 5364 50  0000 C CNN
F 2 "" H 3000 5275 50  0001 C CNN
F 3 "~" H 3000 5275 50  0001 C CNN
	1    3000 5275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5275 3000 5125
Text GLabel 3000 5125 1    50   Output ~ 0
GND
Text GLabel 3000 4675 3    50   Input ~ 0
GND
Wire Wire Line
	3000 4675 3000 4625
Text GLabel 4000 4675 3    50   Input ~ 0
GND
Text GLabel 4975 4700 3    50   Input ~ 0
GND
Wire Wire Line
	4975 4700 4975 4625
Wire Wire Line
	4000 4675 4000 4625
Text GLabel 3000 3775 1    50   Output ~ 0
IN
Wire Wire Line
	3000 3775 3000 4025
Text GLabel 4000 3775 1    50   Output ~ 0
VM
Text GLabel 4975 3775 1    50   Output ~ 0
VP
Wire Wire Line
	4000 3775 4000 4025
Wire Wire Line
	4975 3775 4975 4025
$Comp
L pspice:OPAMP U?
U 1 1 61262ACF
P 4375 2700
F 0 "U?" H 4719 2746 50  0000 L CNN
F 1 "OP07" H 4719 2655 50  0000 L CNN
F 2 "" H 4375 2700 50  0001 C CNN
F 3 "~" H 4375 2700 50  0001 C CNN
F 4 "X" H 4375 2700 50  0001 C CNN "Spice_Primitive"
F 5 "OP07" H 4375 2700 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4375 2700 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "1 2 4 5 3" H 4375 2700 50  0001 C CNN "Spice_Node_Sequence"
F 8 "op07.lib" H 4375 2700 50  0001 C CNN "Spice_Lib_File"
	1    4375 2700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
